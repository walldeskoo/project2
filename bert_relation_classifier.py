import warnings
warnings.simplefilter('ignore')

import numpy as np
import pandas as pd
import os
import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from transformers import AutoTokenizer, AutoModel
from transformers import BertTokenizer, BertModel

from torch.utils.data.dataset import Dataset
from torch.utils.data.dataloader import DataLoader
from tqdm import tqdm
from sklearn.metrics import classification_report,accuracy_score
is_train = False #True: to train model，False:to load trained model and predict on validation and test dataset
model_path = 'bert_relation_classifier.pkl'
max_epoch = 3
batch_size = 50
path = os.getcwd()
device = torch.device('cuda:0') if torch.cuda.is_available() else torch.device('cpu')

class MyDataSet(Dataset):
    def __init__(self,id, text1, text2, labels=None):
        self.text1 = text1
        self.text2 = text2
        self.id = id
        if labels is not None:
            self.labels = labels
        else:
            self.labels = None




    def __getitem__(self, index):
        if self.labels is not None:
            return [self.text1[index],self.text2[index],self.labels[index]]
        else:
            return [self.text1[index],self.text2[index],self.id[index]]

    def __len__(self):
        return len(self.text1)

full_data = pd.read_csv("data/train.csv")

id2label = ['disagreed','unrelated','agreed']
label2id_dict={label:id for id,label in enumerate(id2label)}
full_data['label'] = full_data['label'].map(lambda x:label2id_dict[x])
full_data_shuff = full_data.sample(frac=1, random_state=42)
split_pos = int(len(full_data_shuff)*0.9)
train_data = full_data_shuff.iloc[:split_pos,:]
val_data = full_data_shuff.iloc[split_pos:,:]
#
test_data = pd.read_csv("data/test.csv")



train_dataset = MyDataSet(id=train_data['id'].values,text1=train_data['title1_en'].values,text2=train_data['title2_en'].values,
                          labels=train_data['label'].values)
train_data_loader = DataLoader(train_dataset,batch_size=batch_size,shuffle=True,num_workers=0)
val_dataset = MyDataSet(id=train_data['id'].values,text1=val_data['title1_en'].values,text2=val_data['title2_en'].values,
                          labels=val_data['label'].values)
val_data_loader = DataLoader(val_dataset,batch_size=batch_size,shuffle=False,num_workers=0)

test_dataset = MyDataSet(id=test_data['id'].values,text1=test_data['title1_en'].values,text2=test_data['title2_en'].values)
test_data_loader = DataLoader(test_dataset,batch_size=batch_size,shuffle=False,num_workers=0)


class MYModel(nn.Module):
    def __init__(self):
        super(MYModel, self).__init__()
        emb_dim = 768
        hid_dim = 128
        self.max_length = 100 * 1
        self.tokenizer = BertTokenizer.from_pretrained('bert/bert-base-uncased')
        self.electra_model = BertModel.from_pretrained("bert/bert-base-uncased")
        self.fc1_0 = nn.Linear(emb_dim, hid_dim)
        self.fc2_0 = nn.Linear(hid_dim, 3)
        self.dropout = nn.Dropout(0.5)
        self.criterion = nn.CrossEntropyLoss() #nn.MSELoss()
        # self.is_training = is_training  #True: training mode,False:inference mode

    def make_onehot(self,id,class_num):
        index = id.view(-1,1)
        one_hot = torch.zeros(index.size(0), class_num).to(index.device).scatter(dim=1,index=index,value=1)
        return one_hot

    def forward(self, text1, text2):
        X_inputs = self.tokenizer(text1, text2, padding=True, return_tensors='pt', truncation=True, max_length=self.max_length)
        X_inputs.to(device)
        x = self.electra_model(X_inputs['input_ids'],
            token_type_ids=X_inputs['token_type_ids'],
            attention_mask=X_inputs['attention_mask'],
            return_dict=True)
        x = x.pooler_output
        # x = x[:,0,:]
        x = torch.reshape(x,[-1,x.size(-1)])

        fc1_out0 = F.relu(self.dropout(self.fc1_0(x)))
        out_layer_0 = self.fc2_0(fc1_out0)
        return out_layer_0


def run_train(my_model, train_data_loader):
    electa_params_ids = list(map(id, my_model.electra_model.parameters()))
    base_params = filter(lambda p: id(p) not in electa_params_ids, my_model.parameters())
    optimizer = optim.Adam([{'params': base_params},
                            {'params': my_model.electra_model.parameters(), 'lr': 1e-05}],
                           lr=1e-04)
    best_epoch = 0
    # best_val_score = 0
    min_val_loss = 999999
    for epoch in range(max_epoch):
        my_model.train()
        total_loss = 0
        postfix_dict = {}
        t = tqdm(train_data_loader, desc='Epochs {}/{}'.format(epoch, max_epoch))
        for idx,data_batch in enumerate(t):
            optimizer.zero_grad()
            text1 = list(data_batch[0])
            text2 = list(data_batch[1])
            labels = torch.tensor(list(data_batch[2])).to(device) #torch.stack(data_batch[2],0).float().to(device)
            # texts = torch.tensor(texts).to(device)
            # labels = labels.to(device)
            # input = trainer.X.to(device)
            # trainer.Y = torch.from_numpy(trainer.Y).to(trainer.device)
            outputs = my_model(text1, text2)
            # outputs = torch.stack(outputs,0).squeeze()
            loss = my_model.criterion(outputs,labels)
            total_loss += loss.item()
            postfix_dict['train loss'] = total_loss/(idx+1)
            t.set_postfix(postfix_dict)
            loss.backward()
            optimizer.step()
            # print('train loss={}'.format(loss))
            # dict={'train loss':loss}
            if  (idx+1) % 1000 == 0:
                val_loss,acc,report = run_eval(my_model,val_data_loader)
                print('val loss=', val_loss,'acc',acc)
        val_loss,acc,report = run_eval(my_model, val_data_loader)

        print('val loss=',val_loss)
        print(report)
        if val_loss < min_val_loss:
            min_val_loss = val_loss
            best_epoch = epoch
            print('best model of score {} at epoch {} saved'.format(min_val_loss,epoch))
            torch.save(my_model.state_dict(),model_path)
    print('best epoch=',best_epoch)
    print('min_val_loss=',min_val_loss)



def run_eval(my_model, val_data_loader, has_label = True):
    my_model.eval()
    total_loss = 0
    total_batches = 0
    labels_merged, predicts_merged,ids_merged = [],[],[]
    for idx, data_batch in enumerate(val_data_loader):
        with torch.no_grad():
            text1 = list(data_batch[0])
            # texts_all.extend(text1)
            text2 = data_batch[1]
            # characters_all.extend(text2)
            outputs = my_model(text1, text2)
            predicts = torch.argmax(outputs,1)
            predicts_merged.extend(predicts.cpu().numpy())
            if has_label:#对于有标签的验证集
                labels = torch.tensor(list(data_batch[2])).to(device)
                loss = my_model.criterion(outputs, labels)
                total_loss += loss.item()
                labels_merged.extend(labels.cpu().numpy())
                total_batches += 1
            else: #对于没有标签的测试集
                ids = torch.tensor(list(data_batch[2])).to(device)
                ids_merged.extend(ids.cpu().numpy())


    # print(acc)
    if has_label:
        report = classification_report(labels_merged, predicts_merged)
        acc = accuracy_score(labels_merged, predicts_merged)
        return total_loss/total_batches,acc,report
    else:
        return predicts_merged,ids_merged


my_model = MYModel()
my_model.to(device)
if is_train:

    run_train(my_model,train_data_loader)
else:
    if torch.cuda.is_available():
        model_state = torch.load(model_path)
    else:
        model_state = torch.load(model_path, map_location='cpu')
    my_model.load_state_dict(model_state)
    val_loss, acc, report = run_eval(my_model, val_data_loader,has_label=True)
    print(report)
    predictions,ids = run_eval(my_model, test_data_loader, has_label=False)
    submit = pd.read_csv('sample_submission.csv')
    print(submit.shape)
    print(submit.head())
    sub = submit.copy()
    # predictions_arr =  list(predictions.cpu().numpy())

    sub['label'] = [id2label[pred_id] for pred_id in predictions]
    sub['id'] = ids
    sub.head()
    sub.to_csv('submission_bert.csv',  index=False)
